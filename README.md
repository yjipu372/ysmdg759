#集结号上分找谁信誉好zzd
#### 介绍
集结号上分找谁信誉好【溦:5546055】，集结号游戏上分下分商人微信号是多少【溦:5546055】，集结号上分下分微信【溦:5546055】，集结号上下分银商号是多少【溦:5546055】，集结号上分下分银商【溦:5546055】，　　　　以前的新闻。萨达姆·侯赛因——前伊拉克共和国总统——在他的老家提格里特的一个地窖里被美军俘获。坦白地说，我的感觉是愉快的。我的愉快与美国无关，与布什无关。我的愉快基于我对一个专制、残暴政权和几个丧失人性的人物的认识。希特勒。斯大林。萨达姆的两个儿子乌代和库塞。看见胡子拉杂、神情麻木、苍老憔悴的萨达姆，我想到了命运对人的捉弄。萨达姆算是完蛋了。政治完蛋了，权柄完蛋了，国家蛋完了，连儿孙、家族和自己那条老命也完蛋了。也许有人会说，萨达姆辉煌过。我要说，辉煌不是人生。打电话给诗人雪峰。雪峰正伤心。他是萨达姆的fans。雪峰说，狗日的美国鬼子，连一个老头都不放过。雪峰的声音低沉，悲哀，真实，像是要哭出来。我笑了。笑出了声。“老子算是看清你娃娃了，舔美国人的肥勾子。”雪峰骂我。我没有还击。我又笑了。咯咯咯。雪峰在沉默，半晌才说，这个世道，这个世道……我说，萨达姆是咎由自取。我对雪峰说了萨达姆和他的两个儿子的兽行。雪峰是诗人，早年为阿拉法特写过诗。　　现在，我要说的是另一个人。100%怎么绕也绕不过的人。我向很多人推荐过他。青岛的Ｙ和Ｈ，上海的Ｚ，南京的Ｔ，北京的Ｌ、Ｓ与Ｊ。这个人与我幻想的西伯利亚或古巴的监狱有关，与纳粹有关，与集中营有关，与他胸前佩带过的黄色六角星有关，与奥斯维辛有关。前政府完蛋了，集中营还在，在遥远的圣赫勒拿岛，在对暴政屠杀的记忆中，在《英国旗》上，在《另一个人》里，在《船夫日记》中。这个人叫凯尔泰斯·伊姆莱。匈牙利籍的犹太作家。两小说，两个随笔。“不但揭示了人类恐怖的堕落与沉沦，而且通过文学作品，以一个脆弱易伤的个体与历史进程中的野蛮暴政相抗争”。在我个人的感觉中，凯尔泰斯比卡夫卡要亲切。《英国旗》，一个中篇，对“一种”肉体与精神存在的天才的触摸。大胆，真切。更多的不是叙述，而是表述。这种表述有刀锋的理性，更有芳草和落叶的质感。没有我们时下号召的“好看”，甚至没有传统小说的要素，有的只是个性的絮叨。但这样的絮叨是内心的铺张。包含了人类的良知与道德，包含了人类超乎存在的诗性生活或者人类对诗性生活的渴望，同时也包含了诸多大自然纯洁纯粹的元素。《另一个人》，让我感觉到了灵魂的质量和语言的质量绝佳的共融。“我们不能在自己当过奴役的地方品尝自由”。“我们要尽可能地深远地接受我们的生存”。“我是独裁者无可救药的孩子，打在我神身上的烙印就是我与众不同的地方”。“我喜欢自己身体日趋衰弱的厄运”。“这个世界的渺小，是由日趋严重的脑血管硬化和对自己葬礼的预感造成”的。“我们之所以还能够承受生活，是因为我们的生活竟然如此地不真实”……纳道什·彼特说，世界上有一种痛楚和一种彻底的屈服，只有通过凯尔泰斯的眼睛才能看到。我理解彼特的意思。凯尔泰斯经历了太多的黑暗。　　太阳出来了，山坡的积雪正在融化。“我生存的每个侧面都是令人恐怖的，只有写作的那一部分除外。写作，写作，只不过是为了能忍受自己的生存，还有就是为自己的生存作证”。对于春天的来临，我从来都只有漠然。凯尔泰斯依然活着，在布达佩斯或布拉格。一生仅仅毫厘，在时间的弯曲里，我们看见的不是世界的倒影，而是世界的遗像。“这个世界是从很深很深的地方开始毁灭的，无论借助思想还是科学，历史对它都鞭长莫及”，而我个人“带着特别的苦涩和特别的满足”，写下了这些文字，深深感受到自己存在形式的脆弱易伤、徒劳无益和不合时宜。
　　然后他把她慢慢推远，说祝贺你，考了那么高的分。
https://vk.com/topic-225157404_50383962
https://vk.com/topic-225157884_50384031
https://vk.com/topic-225157404_50384056
https://vk.com/topic-225157884_50384063
https://vk.com/topic-225157404_50384085
https://vk.com/topic-225157884_50384148
https://vk.com/topic-225157884_50384187
https://vk.com/topic-225157404_50384201
https://vk.com/topic-225157884_50384374
https://vk.com/topic-225157404_50384400
https://vk.com/topic-225157404_50384439
https://vk.com/topic-225157884_50384436
https://vk.com/topic-225157884_50384491
https://vk.com/topic-225157404_50384486
https://vk.com/topic-225157404_50384511
https://vk.com/topic-225157884_50384512
https://vk.com/topic-225157884_50384544
https://vk.com/topic-225157884_50384573
https://vk.com/topic-225157404_50384644
https://vk.com/topic-225157884_50384729
https://vk.com/board225157884?act=create
https://vk.com/topic-225157404_50395501
https://vk.com/topic-225157884_50395516
https://vk.com/topic-225157404_50395552
https://vk.com/topic-225157884_50395563
https://vk.com/topic-225157404_50395593
https://vk.com/topic-225157884_50395607
https://vk.com/topic-225157884_50395657
https://vk.com/topic-225157404_50395650
https://vk.com/topic-225157884_50395695

#### 参与贡献
Fork 本仓库
新建 Feat_xxx 分支
提交代码
新建 Pull Request
#### 特技
使用 Readme_XXX.md 来支持不同的语言，例如 Readme_en.md, Readme_zh.md
Gitee 官方博客 blog.gitee.com
你可以 https://gitee.com/explore 这个地址来了解 Gitee 上的优秀开源项目
GVP 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
Gitee 官方提供的使用手册 https://gitee.com/help
Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 https://gitee.com/gitee-stars/